# Electrical Calculator
### Beta 0.15

**Bug Fixes**
* Alert Dialog box before exit confirmation [issue #12](https://gitlab.com/android-development3/electrical-calculator/-/issues/12)
* Alert Dialog box in error pattern [issue #8](https://gitlab.com/android-development3/electrical-calculator/-/issues/8)
* Added Dark Mode in action bar 
* In `Single Phase Calculator` spinner color visibility fixed.
* For testing purpose we used previous app icon.
* Changed image in `About` menu, for testing review.

**Need To Do**

- [ ] How To Use Activity
> Create individual activity for each topic

- [ ] Change `Actionbar` color in `splash screen` based on its background/activity color **[only on this activity]**
- [x] Implement `Enable/Disable` in app **Night/Light** mode.
- [x] [Implement Display error in `Alert Dialog`](https://gitlab.com/android-development3/electrical-calculator/-/issues/8)
- [ ] Implement methods/functionality on `Three Phase Power Calculator` **[postponed]**
- [ ] [Implement new option in main list view name as `electrical all formula` **[postponed]**](https://gitlab.com/android-development3/electrical-calculator/-/issues/11) 
- [ ] Update `Three Phase Power Calculator` layout **[Postponed]**
- [ ] Update `formula` activity layout **[Postponed]**



Square Development Group <br>
Farhan Sadik

