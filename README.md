# Electrical Calculator

| Left columns  | Right columns |
| ------------- |:--------------|
|Started Project| 14-Oct-2021 |
|Released (apk) Version| Beta 0.15 |
|In Development (Version) | [Beta 0.15 ](https://gitlab.com/android-development3/electrical-calculator/-/blob/master/CHANGELOG.md) |
|Status| Work in Progress |
|Project No.| C526|
|Last Updated | 15-Jan-2022 |



> ### Note
>
> This android application currently under development. Released app version was `Beta 0.15`. Now I'm trying to fix some of issues and bugs. Please wait for next release. 



### Status

1. Bugs and Implementation list ~ [gitlab_issues](https://gitlab.com/android-development3/electrical-calculator/-/issues)
2. Changelog, Bug Fixes ~ [view_changelogs](https://gitlab.com/android-development3/electrical-calculator/-/blob/master/CHANGELOG.md)



Download App (***Beta 0.15***) <br>
<a href="https://gitlab.com/android-development3/electrical-calculator/-/blob/master/app/release/ElectricalCalculator_Beta_0.15.apk" rel="some text">![Foo](app/src/main/res/mipmap-hdpi/download_from_cloud.png)</a>



Square Development Group <br>
Farhan Sadik

