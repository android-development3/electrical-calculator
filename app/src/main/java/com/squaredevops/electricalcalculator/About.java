package com.squaredevops.electricalcalculator;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

public class About extends AppCompatActivity {

    Switch aSwitch;
    SharedPreferences sharedPreferences = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setTitle("About");
        /*
        aSwitch = findViewById(R.id.switchMenu);
        aSwitch.setVisibility(View.GONE);

        sharedPreferences = getSharedPreferences("night_mode", MODE_PRIVATE);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                    // night theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES); // setting night theme
                    aSwitch.setChecked(true); // if checked true

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("mode",true); // put-key for save as string
                    editor.apply(); // commit

                } else {

                    // light theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); // setting light theme
                    aSwitch.setChecked(false); // if checked false

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("mode",false); // put-key for save as string
                    editor.apply(); // commit

                }
            }
        });
        */
        // adding back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    // actionbar back button items
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}