package com.squaredevops.electricalcalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

public class ListViewActivity extends AppCompatActivity {

    ListView listView;
    SharedPreferences sharedPreferences = null;
    Switch sw;
    String[] calculation_list = {
            "Single Phase Power Calculator", // "Three Phase Power Calculator",
            "Calculate Resistance"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        listView = (ListView) findViewById(R.id.listviewID);

        ArrayAdapter adapter = new ArrayAdapter<String>(

                // this class
                this,
                // list view layout
                android.R.layout.simple_list_item_1,
                // string
                calculation_list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // list[position] - is the position from list array
                //toastMessage("clicked on " + calculation_list[position]);

                if (calculation_list[position].equals(calculation_list[0])) {
                    Intent intent = new Intent(ListViewActivity.this, SinglePhasePowerCalculator.class);
                    startActivity(intent);
                } else if (calculation_list[position].equals(calculation_list[1])) {
                    Intent intent = new Intent(ListViewActivity.this, ResistanceCalculator.class);
                    startActivity(intent);
                }

                /*
                else if (calculation_list[position].equals(calculation_list[2])) {
                    Intent intent = new Intent(ListViewActivity.this, ThreePhasePowerCalculator.class);
                    startActivity(intent);
                }
                    */
                /*

                THIS IS ALT METHOD

                switch (item.getItemId()) {
                    case R.id.menu1:
                        Toast.makeText(this, "Clicked Menu 1", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.menu2:
                        Toast.makeText(this, "Clicked Menu 2", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                        */

            }
        });

    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        menu.findItem(R.id.how_to_use).setVisible(false);

        MenuItem itemswitch = menu.findItem(R.id.night_mode_switchbar);
        itemswitch.setActionView(R.layout.switch_for_actionbar);

        sw = menu.findItem(R.id.night_mode_switchbar)
                .getActionView()
                .findViewById(R.id.switchFromActionBar);

        sharedPreferences = getSharedPreferences("night_mode", MODE_PRIVATE);

        /*
        // default theme
        Boolean defaultValue = sharedPreferences.getBoolean("mode",true); // string name
        if (defaultValue){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); // night mode
            sw.setChecked(true); // by default
        }
        */

        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    toastMessage("checked");

                    // night theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES); // setting night theme
                    sw.setChecked(true); // if checked true

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("mode", true); // put-key for save as string
                    editor.apply(); // commit

                } else {
                    toastMessage("not checked");

                    // light theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); // setting light theme
                    sw.setChecked(false); // if checked false

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("mode", false); // put-key for save as string
                    editor.apply(); // commit


                }
            }
        });

        return true;
    }

    // actionbar back button items
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        switch (id) {
            case R.id.how_to_use:
                toastMessage("how to use");
                break;
            case R.id.about:
                Intent intent = new Intent(ListViewActivity.this, About.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    // custom toast
    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }

    // confirmation before exit
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true)
                .setIcon(R.drawable.warning)
                .setTitle("Quit")
                .setMessage("Are you sure you want to quit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                        //dialogInterface.cancel();
                    }
                })
                .setNegativeButton("No", null).show();
    }

}