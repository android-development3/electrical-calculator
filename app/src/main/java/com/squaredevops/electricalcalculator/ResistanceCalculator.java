package com.squaredevops.electricalcalculator;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class ResistanceCalculator extends AppCompatActivity {

    EditText current, voltage;
    Button button;
    TextView textView;
    SwipeRefreshLayout swipeRefreshLayout;
    int intCurrent = 0, intVoltage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resistance_calculator);
        setTitle("Resistance");

        current = findViewById(R.id.getCurrent);
        voltage = findViewById(R.id.getVoltage);
        button = findViewById(R.id.calculateWatt);
        textView = findViewById(R.id.printWattCalculationResult);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // Button on action
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getting input as string
                String currentString = current.getText().toString();
                String voltageString = voltage.getText().toString();

                // checking null input
                if (currentString.matches("")) {
                    //textView.setText("Missing Current Value");
                    customAlertDialog("Please enter current value");
                } else if (voltageString.matches("")) {
                    //textView.setText("Missing Voltage Value");
                    customAlertDialog("Please enter voltage value");
                } else {
                    // converting string to integer
                    // current
                    if (!"".equals(currentString)){
                        intCurrent = Integer.parseInt(currentString);
                    }

                    // converting string to integer
                    // voltage
                    if (!"".equals(voltageString)) {
                        intVoltage = Integer.parseInt(voltageString);
                    }

                    // printing result
                    String ohom = " Ω";
                    textView.setText("Resistance = " + intVoltage / intCurrent + ohom);
                } // end of else

            }
        }); // end of button action


        // adding back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Lookup the swipe container view
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                toastMessage("reload");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false); // true = infinity refresh
                        // you must recall (again) the method here for refresh

                        // reload activity
                        finish();
                        startActivity(getIntent());
                    }
                }, 500); // delay time
            }
        });

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        MenuItem itemswitch = menu.findItem(R.id.night_mode_switchbar);
        itemswitch.setActionView(R.layout.switch_for_actionbar);

        final Switch sw = menu.findItem(R.id.night_mode_switchbar)
                .getActionView()
                .findViewById(R.id.switchFromActionBar);
        sw.setVisibility(View.GONE);

        return true;
    }

    // actionbar back button items
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.how_to_use:
                toastMessage("how to use");
                break;
            case R.id.about:
                Intent intent = new Intent(ResistanceCalculator.this, About.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    // custom toast message
    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },1000); // delay time
    }

    // custom alert dialog for view error message
    public void customAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true)
                .setTitle("Error")
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }

}