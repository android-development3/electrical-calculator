package com.squaredevops.electricalcalculator;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class SinglePhasePowerCalculator extends AppCompatActivity {

    EditText current, voltage, pf_editText;
    Spinner pf_spinner;
    Button button;
    TextView textView;
    SwipeRefreshLayout swipeRefreshLayout;

    double currentDecimal = 0;
    double voltageDecimal = 0;
    double pfDecimal = 0, pfDecimalforSpinner = 0, pfDecimalforEdittext = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_phase_power_calculator);
        setTitle("Single Phase Power");

        current = findViewById(R.id.getCurrent);
        voltage = findViewById(R.id.getVoltage);
        pf_spinner = findViewById(R.id.getPowerFector);
        button = findViewById(R.id.calculateWatt);
        textView = findViewById(R.id.printWattCalculationResult);
        pf_editText = findViewById(R.id.getPowerFectorEdittext);

        // disable edittext
        pf_editText.setVisibility(View.GONE);

        String[] pf_values = {"1", ".9", ".85", ".8", "Custom"};
        ArrayAdapter arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_item, pf_values);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pf_spinner.setAdapter(arrayAdapter);
        pf_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // selected text in toast
                //Toast.makeText(getApplicationContext(), pf_values[position], Toast.LENGTH_LONG).show();

                // text color
                ((TextView) pf_spinner.getSelectedView()).setTextColor(Color.GREEN);

                // if spinner = custom then edittext will appear for custom pf value input
                if (pf_spinner.getSelectedItem().toString().equals(pf_values[4])) {
                    pf_editText.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // null
            }
        });

        // button on action
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getting input as string
                String currentString = current.getText().toString();
                String voltageString = voltage.getText().toString();

                // getting from pf
                String pfSpinner = pf_spinner.getSelectedItem().toString();
                String pfEdittext = pf_editText.getText().toString();

                // checking null input
                if (currentString.matches("")) {
                    //textView.setText("Missing Current Value");
                    customAlertDialog("Missing Current Value");
                } else if (voltageString.matches("")) {
                    //textView.setText("Missing Voltage Value");
                    customAlertDialog("Missing Voltage Value");
                } else {

                    // converting String to double
                    // current
                    if(!currentString.equals("")){
                        currentDecimal = Double.parseDouble(currentString);
                    }

                    // converting String to double
                    // voltage
                    if(!voltageString.equals("")){
                        voltageDecimal = Double.parseDouble(voltageString);
                    }

                    // converting String to double
                    // wither edittext neither spinner
                    if (pf_spinner.getSelectedItem().toString().equals(pf_values[4])) {
                        // power factor (edittext)
                        if(!pf_editText.getText().toString().equals("")) {
                            pfDecimal = Double.parseDouble(pf_editText.getText().toString());
                        }
                    } else {
                        // power factor (spinner)
                        if(!pf_spinner.getSelectedItem().toString().equals("")){
                            pfDecimal = Double.parseDouble(pf_spinner.getSelectedItem().toString());
                        }
                    }

                    if (pfDecimal <= 1) {

                        // final result
                        double decimalResult = currentDecimal * voltageDecimal * pfDecimal;;

                        // converting double to integer
                        int integerResult = (int) decimalResult;

                        // printing result
                        if (decimalResult <= 1000) {
                            textView.setText("power = " + integerResult + " watt");
                        } else if (decimalResult >= 1000000) {
                            textView.setText("power = " + decimalResult / 1000000 + " mega watt");
                        } else if (decimalResult > 1000) {
                            textView.setText("power = " + decimalResult / 1000 + " kilo watt");
                        } else {
                            //textView.setText("failed to calculate watt");
                            customAlertDialog("failed to calculate watt");
                            toastMessage("error in x <=/>= calculation");
                        }

                    } else {
                        //textView.setText("you must enter pf > 1");
                        customAlertDialog("you must enter pf > 1");
                        toastMessage("error with pf > 1");
                    }

                }

            }
        });

        // adding back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Lookup the swipe container view
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                toastMessage("reload");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false); // true = infinity refresh
                        // you must recall (again) the method here for refresh

                        // reload activity
                        finish();
                        startActivity(getIntent());
                    }
                }, 500); // delay time
            }
        });

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.how_to_use).setTitle("Formula");

        MenuItem itemswitch = menu.findItem(R.id.night_mode_switchbar);
        itemswitch.setActionView(R.layout.switch_for_actionbar);

        final Switch sw = menu.findItem(R.id.night_mode_switchbar)
                .getActionView()
                .findViewById(R.id.switchFromActionBar);
        sw.setVisibility(View.GONE);

        return true;
    }

    // actionbar back button items
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.how_to_use:
                toastMessage("Formula");
                startActivity(new Intent(getApplicationContext(), FormulaForSinglePhasePowerCalculator.class));
                break;
            case R.id.about:
                startActivity(new Intent(getApplicationContext(), About.class));
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    // custom toast message
    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },1000); // delay time
    }

    // custom alert dialog for view error message
    public void customAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true)
                .setTitle("Error")
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                }).show();
    }
}
